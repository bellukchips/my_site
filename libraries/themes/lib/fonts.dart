import 'package:flutter/material.dart';


class Fonts {
  static TextStyle defaultStyle = TextStyle(fontFamily: 'Poppins');
  static TextStyle caveatStyle = TextStyle(fontFamily: 'Caveat');
  static TextStyle cutiveMonoStyle  = TextStyle(fontFamily: 'CutiveMono');
  static TextStyle montserratMedium  = TextStyle(fontFamily: 'Montserrat Medium');
  static TextStyle montserratSemiBold  = TextStyle(fontFamily: 'Montserrat SemiBold');
  static TextStyle montserratBold  = TextStyle(fontFamily: 'Montserrat Bold');
}
