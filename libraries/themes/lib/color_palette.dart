import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';

class ColorPallette {
  //
  static Color primaryColor = '46DEFF'.toColor();
  static Color secondColor = '38D0F1'.toColor();
  static Color redColor = "C81E1E".toColor();
  static Color white = Colors.white;
}
