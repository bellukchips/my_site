part of 'widgets.dart';

class ButtonDownloadCV extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ButtonStyle(
          side: MaterialStateProperty.all(
            BorderSide(color: ColorPallette.primaryColor, width: 1),
          ),
          backgroundColor: MaterialStateProperty.all(
            ColorPallette.white,
          ),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))))),
      onPressed: () {
        Surel.launchUrlDownloadCV();
      },
      child: Text(
        'Download CV',
        style: Fonts.cutiveMonoStyle.copyWith(
            color: ColorPallette.secondColor, fontWeight: FontWeight.bold),
      ),
    );
  }
}
