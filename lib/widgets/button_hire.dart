part of 'widgets.dart';

class ButtonHire extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            ColorPallette.primaryColor,
          ),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))))),
      onPressed: () {
        Surel.openEmail(
            toEmail: "muhamadlukman937@gmail.com",
            subject: "Hire Pekerjaan (Bisa diganti)",
            body: "");
      },
      child: Text(
        'Hire Me',
        style: Fonts.cutiveMonoStyle
            .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}
