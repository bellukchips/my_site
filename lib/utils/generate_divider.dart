part of 'utils.dart';

Widget generateDashedDivider(double height) {
  int n = height ~/ 5;
  return Column(
    children: List.generate(
        n,
        (index) => (index % 2 == 0)
            ? Container(
                height: 10,
                width: 2,
                color: ColorPallette.white,
              )
            : SizedBox(
                height: height / n,
              )),
  );
}
