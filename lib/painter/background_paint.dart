part of 'painter.dart';

class BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(0, 0);
    path_0.lineTo(size.width, 0);
    path_0.lineTo(size.width, size.height * 0.9252579);
    path_0.lineTo(size.width * 0.9661850, size.height * 0.8718553);
    path_0.lineTo(size.width * 0.9294798, size.height * 0.8152516);
    path_0.lineTo(size.width * 0.8895954, size.height * 0.7586478);
    path_0.lineTo(size.width * 0.8540462, size.height * 0.7130503);
    path_0.lineTo(size.width * 0.8095376, size.height * 0.6623711);
    path_0.lineTo(size.width * 0.7855491, size.height * 0.6422956);
    path_0.lineTo(size.width * 0.7650289, size.height * 0.6328616);
    path_0.lineTo(size.width * 0.7517341, size.height * 0.6288664);
    path_0.lineTo(size.width * 0.7401734, size.height * 0.6271478);
    path_0.lineTo(size.width * 0.7309249, size.height * 0.6262893);
    path_0.lineTo(size.width * 0.7228324, size.height * 0.6262893);
    path_0.lineTo(size.width * 0.7066474, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6965318, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6881503, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6800578, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6734104, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6647399, size.height * 0.6257862);
    path_0.lineTo(size.width * 0.6528902, size.height * 0.6262893);
    path_0.lineTo(size.width * 0.6427746, size.height * 0.6271478);
    path_0.lineTo(size.width * 0.6286127, size.height * 0.6305849);
    path_0.lineTo(size.width * 0.6130058, size.height * 0.6357390);
    path_0.lineTo(size.width * 0.6000000, size.height * 0.6415094);
    path_0.lineTo(size.width * 0.5846821, size.height * 0.6494843);
    path_0.lineTo(size.width * 0.5696532, size.height * 0.6597940);
    path_0.lineTo(size.width * 0.5554913, size.height * 0.6713836);
    path_0.lineTo(size.width * 0.5424855, size.height * 0.6847484);
    path_0.lineTo(size.width * 0.5294798, size.height * 0.6993129);
    path_0.lineTo(size.width * 0.5164740, size.height * 0.7147767);
    path_0.lineTo(size.width * 0.4640173, size.height * 0.7757736);
    path_0.lineTo(size.width * 0.4236994, size.height * 0.8264607);
    path_0.lineTo(size.width * 0.3644509, size.height * 0.8977657);
    path_0.lineTo(size.width * 0.3494220, size.height * 0.9158082);
    path_0.lineTo(size.width * 0.3271676, size.height * 0.9372846);
    path_0.lineTo(size.width * 0.3080925, size.height * 0.9553270);
    path_0.lineTo(size.width * 0.2884393, size.height * 0.9690723);
    path_0.lineTo(size.width * 0.2710983, size.height * 0.9776635);
    path_0.lineTo(size.width * 0.2494220, size.height * 0.9871132);
    path_0.lineTo(size.width * 0.2095376, size.height * 0.9982814);
    path_0.lineTo(size.width * 0.2026012, size.height);
    path_0.lineTo(size.width * 0.1979769, size.height);
    path_0.lineTo(size.width * 0.1867052, size.height * 0.9982814);
    path_0.lineTo(size.width * 0.1699422, size.height * 0.9939858);
    path_0.lineTo(size.width * 0.1465318, size.height * 0.9871132);
    path_0.lineTo(size.width * 0.1086705, size.height * 0.9699308);
    path_0.lineTo(size.width * 0.08583815, size.height * 0.9561855);
    path_0.lineTo(size.width * 0.06560694, size.height * 0.9432987);
    path_0.lineTo(size.width * 0.04884393, size.height * 0.9304119);
    path_0.lineTo(size.width * 0.03352601, size.height * 0.9149481);
    path_0.lineTo(size.width * 0.02052023, size.height * 0.8977657);
    path_0.lineTo(size.width * 0.01184971, size.height * 0.8831619);
    path_0.lineTo(0, size.height * 0.8616840);
    path_0.lineTo(0, 0);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff38D0F1).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
