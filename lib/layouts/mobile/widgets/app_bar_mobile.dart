import 'package:flutter/material.dart';
import 'package:themes/theme.dart';

class AppBarMobile extends StatelessWidget implements PreferredSizeWidget {
  final double opacity;
  AppBarMobile(this.opacity) : preferredSize = Size.fromHeight(60);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      // backgroundColor: Theme.of(context).bottomAppBarColor.withOpacity(opacity),
      backgroundColor: ColorPallette.secondColor.withOpacity(opacity),
      elevation: 0,
      centerTitle: true,
      title: Text(
        "Lukman",
        style: Fonts.caveatStyle.copyWith(
          fontSize: 40,
          color: ColorPallette.white,
          fontWeight: FontWeight.bold,
          letterSpacing: 3,
        ),
      ),
    );
  }

  @override
  final Size preferredSize;
}
