
import 'package:flutter/material.dart';
import 'package:my_site/utils/utils.dart';
import 'package:themes/theme.dart';

class MenuDrawer extends StatefulWidget {
  @override
  _MenuDrawerState createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 2,
      child: Container(
        color: ColorPallette.primaryColor,
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 50),
                        child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: ColorPallette.white,
                            size: 40,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      MenuSideBar(
                        onTap: () {},
                        title: "Download CV",
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 29),
                        child: generateDashedDivider(50),
                      ),
                      MenuSideBar(
                        onTap: () {},
                        title: "Portofolio",
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 29),
                        child: generateDashedDivider(50),
                      ),
                      MenuSideBar(
                        onTap: () {},
                        title: "About Me",
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 29),
                        child: generateDashedDivider(50),
                      ),
                      MenuSideBar(
                        onTap: () {},
                        title: "Contact Us",
                      ),
                     
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

class MenuSideBar extends StatelessWidget {
  final Function onTap;
  final String title;

  const MenuSideBar({Key key, this.onTap, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: ColorPallette.white,
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          InkWell(
            onTap: () {
              if (onTap != null) {
                onTap();
              }
            },
            child: Text(title,
                style: Fonts.cutiveMonoStyle.copyWith(
                    fontSize: 15,
                    fontWeight: FontWeight.w300,
                    color: ColorPallette.white)),
          ),
        ],
      ),
    );
  }
}
