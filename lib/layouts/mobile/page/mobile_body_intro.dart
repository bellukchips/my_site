import 'package:flutter/material.dart';
import 'package:my_site/widgets/widgets.dart';
import 'package:themes/theme.dart';

class MobileBodyIntro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ///image phone
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 80),
              height: 300,
              width: 250,
              child: Image.asset("assets/phone.png"),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 50),
            child: Text(
              "Build applications with\na beautiful appearance and\neasy to use by users",
              style: Fonts.montserratSemiBold.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: ColorPallette.primaryColor),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ///button hire
              CustomElevation(
                height: 35,
                color: Colors.blue[800],
                child: Container(width: 130, child: ButtonHire()),
              ),
              SizedBox(
                width: 20,
              ),

              ///button portofolio
              CustomElevation(
                height: 35,
                color: Colors.black,
                child: Container(width: 130, child: ButtonDownloadCV()),
              ),
            ],
          ),
          Container(
            // margin: EdgeInsets.only(top: 20),
            height: 200,
            width: 500,
            child: Image.asset("assets/skill_2.png"),
          )
        ],
      ),
    );
  }
}
