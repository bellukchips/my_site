import 'package:flutter/material.dart';
import 'package:my_site/widgets/widgets.dart';
import 'package:themes/theme.dart';

class TabletBodyIntro extends StatefulWidget {
  @override
  _TabletBodyIntroState createState() => _TabletBodyIntroState();
}

class _TabletBodyIntroState extends State<TabletBodyIntro> {
  @override
  Widget build(BuildContext context) {
    var marginTop = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(40, marginTop / 4, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    "Build applications with\na beautiful appearance and\neasy to use by users",
                    style: Fonts.montserratSemiBold.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: ColorPallette.white)),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    ///button hire
                    CustomElevation(
                      height: 35,
                      color: Colors.blue[800],
                      child: Container(
                        width: 130,
                        child: ButtonHire()
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),

                    ///button portofolio
                    CustomElevation(
                      height: 35,
                      color: Colors.black,
                      child: Container(width: 130, child: ButtonDownloadCV()),
                    ),
                  ],
                ),
                //image skill
                Container(
                  margin: EdgeInsets.only(top: 120),
                  height: 150,
                  width: 320,
                  child: Image.asset("assets/skill.png"),
                )
              ],
            ),
          ),

          ///image phone
          Container(
            height: 330,
            width: 330,
            child: Image.asset("assets/phone.png"),
          )
        ],
      ),
    );
  }
}
