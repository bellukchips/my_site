import 'package:flutter/material.dart';
import 'package:my_site/layouts/mobile/page/mobile_body_intro.dart';
import 'package:my_site/utils/utils.dart';
import 'package:my_site/widgets/widgets.dart';
import 'package:my_site/layouts/layouts.dart';
import 'package:themes/theme.dart';

class MediumScreenIntro extends StatefulWidget {
  @override
  _MediumScreenIntroState createState() => _MediumScreenIntroState();
}

class _MediumScreenIntroState extends State<MediumScreenIntro> {
  @override
  Widget build(BuildContext context) {
    var marginTop = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ///
          /// motto
          Container(
            padding: EdgeInsets.fromLTRB(40, marginTop / 5, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    "Build applications with\na beautiful appearance and\neasy to use by users",
                    style: Fonts.montserratSemiBold.copyWith(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: ColorPallette.white)),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    ///button hire
                    CustomElevation(
                      height: 35,
                      color: Colors.blue[800],
                      child: Container(width: 150, child: ButtonHire()),
                    ),
                    SizedBox(
                      width: 20,
                    ),

                    ///button portofolio
                    CustomElevation(
                      height: 35,
                      color: Colors.black,
                      child: Container(width: 150, child: ButtonDownloadCV()),
                    ),
                  ],
                ),
                //image skill
                Container(
                  margin: EdgeInsets.only(top: 140),
                  height: 150,
                  width: 420,
                  child: Image.asset("assets/skill.png"),
                )

                ///
              ],
            ),
          ),

          ///image phone
          Container(
            height: 330,
            width: 330,
            child: Image.asset("assets/phone.png"),
          )
        ],
      ),
    );
  }
}
