import 'package:flutter/material.dart';
import 'package:my_site/layouts/mobile/page/mobile_body_intro.dart';
import 'package:my_site/utils/utils.dart';
import 'package:my_site/widgets/widgets.dart';
import 'package:my_site/layouts/layouts.dart';
import 'package:themes/theme.dart';

class IntroWidget extends StatelessWidget {
  final double screenSize;
  final double marginTop;

  const IntroWidget({Key key, this.screenSize, this.marginTop})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenSize,
      child: BackgroundHome(
        child: ResponsiveWidget.isSmallScreen(context)
            ? MobileBodyIntro()
            : ResponsiveWidget.isSecondSmallScreen(context)
                ? TabletBodyIntro()
                : ResponsiveWidget.isMediumScreen(context)
                    ? MediumScreenIntro()
                    : WebBodyIntro(marginTop: marginTop),
      ),
    );
  }
}

class WebBodyIntro extends StatelessWidget {
  const WebBodyIntro({
    Key key,
    @required this.marginTop,
  }) : super(key: key);

  final double marginTop;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ///
          /// motto
          Container(
            padding: EdgeInsets.fromLTRB(20, marginTop, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    "Build applications with\na beautiful appearance and\neasy to use by users",
                    style: Fonts.montserratSemiBold.copyWith(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: ColorPallette.white)),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    ///button hire
                    CustomElevation(
                      height: 35,
                      color: Colors.blue[800],
                      child: Container(width: 150, child: ButtonHire()),
                    ),
                    SizedBox(
                      width: 20,
                    ),

                    ///button portofolio
                    CustomElevation(
                      height: 35,
                      color: Colors.black,
                      child: Container(width: 150, child: ButtonDownloadCV()),
                    ),
                  ],
                ),
                //image skill
                Container(
                  margin: EdgeInsets.only(top: 80),
                  height: 150,
                  width: 450,
                  child: Image.asset("assets/skill.png"),
                )

                ///
              ],
            ),
          ),

          ///image phone
          Container(
            height: 430,
            width: 430,
            margin: EdgeInsets.only(right: 50),
            child: Image.asset("assets/phone.png"),
          )
        ],
      ),
    );
  }
}
