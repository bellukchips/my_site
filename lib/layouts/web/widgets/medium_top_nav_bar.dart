import 'package:flutter/material.dart';
import 'package:themes/theme.dart';

class MediumTopNavBar extends StatefulWidget {
  final double opacity;
  MediumTopNavBar({this.opacity});
  @override
  _MediumTopNavBarState createState() => _MediumTopNavBarState();
}

class _MediumTopNavBarState extends State<MediumTopNavBar> {
  //hovering menu
  final List _isHovering = [
    false,
    false,
    false,
    false,
    false,
    // false,
    // false,
    // false
  ];

  // bool _isProcessing = false;
  @override
  Widget build(BuildContext context) {
    //screen size
    var screenSize = MediaQuery.of(context).size;
    return PreferredSize(
      preferredSize: Size(screenSize.width, 1000),
      child: Container(
        color: ColorPallette.secondColor.withOpacity(widget.opacity),
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {},
                hoverColor: Colors.transparent,
                child: Text('Lukman',
                    style: Fonts.montserratBold.copyWith(
                      fontSize: 25,
                      color: ColorPallette.white,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 3,
                    )),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(width: screenSize.width / 8),
                    InkWell(
                      onHover: (value) {
                        setState(() {
                          value
                              ? _isHovering[0] = true
                              : _isHovering[0] = false;
                        });
                      },
                      onTap: () {},
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 40,
                            width: 120,
                            decoration: BoxDecoration(
                                color: _isHovering[0]
                                    ? ColorPallette.white
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Center(
                              child: Text('Greeting Cards',
                                  style: Fonts.montserratMedium.copyWith(
                                      fontSize: 13,
                                      color: _isHovering[0]
                                          ? ColorPallette.secondColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 50),
                    InkWell(
                      onHover: (value) {
                        setState(() {
                          value
                              ? _isHovering[1] = true
                              : _isHovering[1] = false;
                        });
                      },
                      onTap: () {},
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                                color: _isHovering[1]
                                    ? ColorPallette.white
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Center(
                              child: Text('Portofolio',
                                  style: Fonts.montserratMedium.copyWith(
                                      fontSize: 13,
                                      color: _isHovering[1]
                                          ? ColorPallette.secondColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 50),
                    InkWell(
                      onHover: (value) {
                        setState(() {
                          value
                              ? _isHovering[2] = true
                              : _isHovering[2] = false;
                        });
                      },
                      onTap: () {},
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                                color: _isHovering[2]
                                    ? ColorPallette.white
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Center(
                              child: Text('About Me',
                                  style: Fonts.montserratMedium.copyWith(
                                      fontSize: 13,
                                      color: _isHovering[2]
                                          ? ColorPallette.secondColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 50),
                    InkWell(
                      onHover: (value) {
                        setState(() {
                          value
                              ? _isHovering[3] = true
                              : _isHovering[3] = false;
                        });
                      },
                      onTap: () {},
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                                color: _isHovering[3]
                                    ? ColorPallette.white
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Center(
                              child: Text('Contact Us',
                                  style: Fonts.montserratMedium.copyWith(
                                      fontSize: 13,
                                      color: _isHovering[3]
                                          ? ColorPallette.secondColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
