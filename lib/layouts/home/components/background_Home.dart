part of '../home.dart';

class BackgroundHome extends StatelessWidget {
  final Widget child;
  BackgroundHome({@required this.child});
  @override
  Widget build(BuildContext context) {
    return ResponsiveWidget.isSmallScreen(context)
        ? Stack(
            children: [
              // Positioned(
              //   top: 0,
              //   right: 0,
              //   child: CustomPaint(
              //     size: Size(100, 130),
              //     painter: TopPainter(),
              //   ),
              // ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   child: CustomPaint(
              //     size: Size(100, 100),
              //     painter: BottomPainter(),
              //   ),
              // ),
              Container(
                // width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height * 0.35,
                child: CustomPaint(
                  painter: BackgroundPainter(),
                  size: Size(MediaQuery.of(context).size.width, 400),
                ),
              ),
              child
            ],
          )
        : Stack(
            children: [
              // Positioned(
              //   top: 0,
              //   right: 0,
              //   child: CustomPaint(
              //     size: Size(150, 200),
              //     painter: TopPainter(),
              //   ),
              // ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   child: CustomPaint(
              //     size: Size(120, 120),
              //     painter: BottomPainter(),
              //   ),
              // ),
              Container(
                // width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height * 0.45,
                child: CustomPaint(
                  painter: BackgroundPainter(),
                  size: Size(MediaQuery.of(context).size.width, 450),
                ),
              ),
              child
            ],
          );
  }
}
