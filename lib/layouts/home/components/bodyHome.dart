part of './../home.dart';

class BodyHome extends StatefulWidget {
  final ScrollController scrollController;
  final double screenSize;
  final double heightIntro;

  const BodyHome(
      {Key key, this.scrollController, this.screenSize, this.heightIntro})
      : super(key: key);
  @override
  _BodyHomeState createState() => _BodyHomeState();
}

class _BodyHomeState extends State<BodyHome> {
  @override
  Widget build(BuildContext context) {
    return WebScrollbar(
      color: Colors.grey,
      backgroundColor: Colors.blueGrey.withOpacity(0.3),
      width: 10,
      heightFraction: 0.3,
      controller: widget.scrollController,
      child: SingleChildScrollView(
        controller: widget.scrollController,
        physics: ClampingScrollPhysics(),
        child: Container(
          child: Column(
            children: [
              IntroWidget(
                screenSize: widget.heightIntro,
                marginTop: widget.screenSize,
              ),
              IntroWidget(
                screenSize: widget.heightIntro,
                marginTop: widget.screenSize,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
