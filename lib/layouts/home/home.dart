import 'package:flutter/material.dart';
import 'package:my_site/layouts/layouts.dart';
import 'package:my_site/painter/painter.dart';
import 'package:my_site/utils/utils.dart';
import 'package:themes/color_palette.dart';
import 'package:themes/theme.dart';
part 'components/background_Home.dart';
part 'components/bodyHome.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _scrollController;
  double _scrollPosition = 0;
  double _opacity = 0;

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    _opacity = _scrollPosition < screenSize.height * 0.40
        ? _scrollPosition / (screenSize.height * 0.40)
        : 1;
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: ColorPallette.white,
        appBar: ResponsiveWidget.isSmallScreen(context) ||
                ResponsiveWidget.isSecondSmallScreen(context)
            ? AppBarMobile(_opacity)
            : ResponsiveWidget.isMediumScreen(context)
                ? PreferredSize(
                    preferredSize: Size(screenSize.width, 1000),
                    child: MediumTopNavBar(
                      opacity: _opacity,
                    ),
                  )
                : PreferredSize(
                    preferredSize: Size(screenSize.width, 1000),
                    child: TopNavBar(
                      opacity: _opacity,
                    ),
                  ),
        endDrawer: MenuDrawer(),
        body: Container(
            child: BodyHome(
          screenSize: screenSize.height / 3,
          scrollController: _scrollController,
          heightIntro: ResponsiveWidget.isSmallScreen(context)
              ? screenSize.height + 200 :  ResponsiveWidget.isSecondSmallScreen(context)  ? screenSize.height + 10
              : ResponsiveWidget.isMediumScreen(context)
                  ? screenSize.height + 50
                  : screenSize.height,
        )));
  }
}

//🧟