import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Surel {
  static Future launchUrlDownloadCV() async {
    const url =
        "https://drive.google.com/u/0/uc?id=1xH8rwBYtPPuqMojGU0hFWN9F0xE4unHV&export=download";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static Future openEmail({
    @required String toEmail,
    @required String subject,
    @required String body,
  }) async {
    final url =
        'mailto:$toEmail?subject=${Uri.encodeFull(subject)}&body=${Uri.encodeFull(body)}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
